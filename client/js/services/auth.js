materialAdmin


  .factory('AuthService', ['User', '$q', '$rootScope', '$window', function(User, $q,
      $rootScope, $window) {
    function login(email, password) {  
      return User
        .login(
          {
            email: email, 
            password: password,
            fields: {
              username: true
            }
          }
        )
        .$promise
        .then(function(response) {
          $window.localStorage['$LoopBack$currentUsername'] = response.user.username;
          $rootScope.currentUser = {
            id: response.user.id,
            tokenId: response.id,
            email: email
          };
        });
    }

    function logout() {
      return User
       .logout()
       .$promise
       .then(function() {
            $window.localStorage['$LoopBack$currentUsername'] = '';
            $rootScope.currentUser = null;
       });
    }

    function register(email, password) {
      return User
        .create({
         email: email,
         password: password
       })
       .$promise;
    }

    return {
      login: login,
      logout: logout,
      register: register
    };
  }]);