materialAdmin

// =========================================================================
// Article Controller
// =========================================================================
.controller('ArticleController', ['$sce', '$compile', '$scope', '$state', 'createChangeStream', 'LiveSet', '$stateParams', 'Article', 'growlService', 'FileUploader',
    function($sce, $compile, $scope, $state, createChangeStream, LiveSet, $stateParams, Article, growlService, FileUploader) {
        'use strict';

        var ctrl = this;
        ctrl.articles = null;
        ctrl.widgets = [];
        ctrl.article = {
            title: '',
            excerpt: '',
            widgets: []
        };
        ctrl.totalItems = null;
        ctrl.currentPage = 1;
        ctrl.perPage = 10;

        // create a uploader with options
        var uploader = ctrl.uploader = new FileUploader({
            scope: $scope, // to automatically update the html. Default: $rootScope
            url: '/api/containers/container1/upload',
            formData: [{
                key: 'value'
            }]
        });
        uploader.onAfterAddingFile = function(item) {
          console.info('After adding a file', item.purpose);
          console.info(ctrl.uploader.queue.length);
        };

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        }

        ctrl.setPage = function(pageNo) {
            ctrl.currentPage = pageNo;
            ctrl.getArticles();
        };

        ctrl.getArticles = function() {
            Article.count()
                .$promise
                .then(function(result) {
                    ctrl.totalItems = result.count;
                    Article.find({
                        filter: {
                            limit: ctrl.perPage,
                            skip: ctrl.perPage * ctrl.currentPage - ctrl.perPage,
                            fields: {
                                id: true,
                                title: true,
                                slug: true,
                                featured: true,
                                excerpt: true,
                                widgets: true,
                                createdAt: true
                            },
                            order: 'createdAt DESC'
                        }
                    }, function(response) {
                        ctrl.articles = response;
                    });
                });
        };

        ctrl.getArticle = function() {
            Article
                .findById({
                    id: $stateParams.id
                })
                .$promise
                .then(function(article) {
                    ctrl.article = article;
                });
        };

        ctrl.addArticle = function() {
            if (uploader.queue.length) {                
                uploader.uploadAll();
                uploader.onCompleteItem = function(item, response, status, headers) {
                    item.purpose === "featured" ? ctrl.article.featured = '/api/containers/container1/download/' + item.file.name : '';
                };
                uploader.onCompleteAll = function() {
                    ctrl.upsert();
                };
            } else {
                ctrl.upsert();
            }
        };

        ctrl.updateArticle = function() {
            if (uploader.queue.length) {
                uploader.uploadAll();
                uploader.onCompleteItem = function(item, response, status, headers) {
                    ctrl.article.featured = '/api/containers/container1/download/' + item.file.name;
                };
                uploader.onCompleteAll = function() {
                    ctrl.upsert();
                };
            } else {
                ctrl.upsert();
            }
        };

        ctrl.upsert = function() {
            // console.log(ctrl.article); return false;
            Article
                .upsert(ctrl.article)
                .$promise
                .then(function() {
                    growlService.growl('Success!', 'success');
                    $state.go("articles.list");
                }, function(response) {
                    growlService.growl(response.status + ': ' + response.statusText + '!', 'danger');
                });
        };

        ctrl.removeArticle = function(item) {
            Article
                .deleteById(item)
                .$promise
                .then(function() {
                    ctrl.articles.splice(ctrl.articles.indexOf(item), 1);
                    growlService.growl('Article deleted!', 'success');
                });
        };

        ctrl.addWidget = function(type) {
            let widget = {
                type: type,
                content: null
            };
            ctrl.article.widgets.push(widget);
            console.log(ctrl.article.widgets);
        };

        ctrl.removeWidget = function(widget) {
            ctrl.article.widgets.splice(ctrl.article.widgets.indexOf(widget), 1);
        };

        /*Wall*/
        ctrl.wallArticles = null;
        ctrl.infiniteArticles = [];
        ctrl.infiniteScrollDisabled = false;
        

        ctrl.getWallArticles = function() {
            ctrl.infiniteScrollDisabled = true;
            Article.find({
                filter: {
                    limit: ctrl.perPage,
                    skip: ctrl.perPage * ctrl.currentPage - ctrl.perPage,
                    fields: {
                        id: true,
                        title: true,
                        slug: true,
                        featured: true,
                        excerpt: true,
                        widgets: true,
                        createdAt: true
                    },
                    order: 'createdAt DESC'
                }
            }).$promise.then(function(response) {
                var changeStreamUrl = '/api/articles/change-stream';
                var src = new EventSource(changeStreamUrl);
                var changes = createChangeStream(src);
                var set;
                set = new LiveSet(response, changes);
                ctrl.wallArticles = set.toLiveArray();
                ctrl.infiniteScrollDisabled = false;
            });
        };

        ctrl.scrollPagin = function() {
            ctrl.infiniteScrollDisabled = true;
            ctrl.currentPage++;
            Article.find({
                filter: {
                    limit: ctrl.perPage,
                    skip: ctrl.perPage * ctrl.currentPage - ctrl.perPage,
                    fields: {
                        id: true,
                        title: true,
                        slug: true,
                        featured: true,
                        excerpt: true,
                        widgets: true,
                        createdAt: true
                    },
                    order: 'createdAt DESC'
                }
            }).$promise.then(function(response) {
                angular.forEach(response, function(article) {
                    ctrl.infiniteArticles.push(article);
                });
                response.length > 0 ? ctrl.infiniteScrollDisabled = false : '';
            });
        };
    }
]);