"use strict";
materialAdmin

// =========================================================================
// Auth Controller
// =========================================================================

.controller('AuthLoginController', ['$window', '$rootScope', '$scope', 'AuthService', '$state', 'growlService', '$cookies', 'User', 'LoopBackAuth', '$stateParams', '$location',  
    function($window, $rootScope, $scope, AuthService, $state, growlService, $cookies, User, LoopBackAuth, $stateParams, $location) {
        var cookies = $cookies.getAll();
        var currentUserId = cookies['userId'];
        var accessTokenId = cookies['access_token'];
        if (currentUserId && accessTokenId) {
            if (!$window.localStorage['$LoopBack$accessTokenId']) {
                LoopBackAuth.currentUserId = currentUserId;
                LoopBackAuth.accessTokenId = accessTokenId;
                LoopBackAuth.rememberMe = true;
                LoopBackAuth.save();
                console.log('after', LoopBackAuth);
                if ($rootScope.intended && $rootScope.intended !== '') {
                        let intended = $rootScope.intended;
                        $rootScope.intended = '';
                        $state.go(intended);
                    } else {
                        $state.go('home');
                    }
                    //Welcome Message
                    growlService.growl('Welcome back ' + $window.localStorage['$LoopBack$currentUsername'] + '!', 'inverse')
            }
            // User
            //     .getCurrent()
            //     .$promise
            //     .then(function(user) {
            //         console.log('email',user.email);
            //         $rootScope.currentUser = $scope.user = user;
            //     })
        }

        //Status

        this.login = 1;
        this.register = 0;
        this.forgot = 0;
        $scope.user = {
            email: '',
            password: ''
        };

        $scope.login = function() {
            AuthService.login($scope.user.email, $scope.user.password)
                .then(function() {
                    if ($rootScope.intended && $rootScope.intended !== '') {
                        let intended = $rootScope.intended;
                        $rootScope.intended = '';
                        $state.go(intended);
                    } else {
                        $state.go('home');
                    }
                    //Welcome Message
                    growlService.growl('Welcome back ' + $window.localStorage['$LoopBack$currentUsername'] + '!', 'inverse')
                }, function(response) {
                    //Error Message
                    growlService.growl(response.status + ': ' + response.statusText + '!', 'danger');
                });
        };
    }
])
    .controller('AuthLogoutController', ['$rootScope', '$scope', 'AuthService', '$state',
        function($rootScope, $scope, AuthService, $state) {
            AuthService.logout()
                .then(function() {
                    $rootScope.intended = '';
                    $state.go('login');
                });
        }
    ])
    .controller('SignUpController', ['$scope', 'AuthService', '$state',
        function($scope, AuthService, $state) {
            $scope.user = {
                email: 'baz@qux.com',
                password: 'bazqux'
            };

            $scope.register = function() {
                AuthService.register($scope.user.email, $scope.user.password)
                    .then(function() {
                        $state.transitionTo('sign-up-success');
                    });
            };
        }
    ]);