'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:AccountCtrl
 * @description
 * # AccountCtrl
 * Controller of the appApp
 */
materialAdmin
  .controller('AccountCtrl', function ($scope, $rootScope, $cookies, User, LoopBackAuth, $stateParams, $location) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var cookies = $cookies.getAll();
    var currentUserId = cookies['userId'];
    var accessTokenId = cookies['access_token'];

    currentUserId = currentUserId.split('.')[0];
    currentUserId = currentUserId.split(':')[1];

    accessTokenId = accessTokenId.split('.')[0];
    accessTokenId = accessTokenId.split(':')[1];

    console.log('before',LoopBackAuth);
    if( localStorage.getItem('$LoopBack$accessTokenId') == null ){
      LoopBackAuth.currentUserId = currentUserId;
      LoopBackAuth.accessTokenId = accessTokenId;
      LoopBackAuth.rememberMe = true;
      LoopBackAuth.save();
      console.log('after',LoopBackAuth);
    }
    

    User
    .getCurrent()
    .$promise
    .then(function(user){
      console.log(user);
      $rootScope.currentUser = $scope.user = user;
    })
  });