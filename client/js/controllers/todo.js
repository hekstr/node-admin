materialAdmin
	
	

	// =========================================================================
  // Todo Controller
  // =========================================================================
  .controller('TodoController', ['$scope', '$state', 'Todo', function($scope,
      $state, Todo) {
  	var ctrl = this;
    ctrl.todos = [];
    this.getTodos = function() {
      Todo
        .find()
        .$promise
        .then(function(results) {
          ctrl.todos = results;
        });
    };
    ctrl.getTodos();

    ctrl.addTodo = function() {
      Todo
        .create($scope.newTodo)
        .$promise
        .then(function(todo) {
          $scope.newTodo = '';
          $scope.todoForm.content.$setPristine();
          $('.focus').focus();
          ctrl.getTodos();
        });
    };

    ctrl.removeTodo = function(item) {
      Todo
        .deleteById(item)
        .$promise
        .then(function() {
          ctrl.getTodos();
        });
    };
  }]);