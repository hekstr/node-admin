materialAdmin

// =========================================================================
// <div>{{'' | currentdate}}</div>
// =========================================================================
.filter('currentdate',['$filter',  function($filter) {
    return function() {
        return $filter('date')(new Date(), 'yyyy-MM-dd H:m');
    };
}])
.filter('unsafe', function($sce) { return $sce.trustAsHtml; });