materialAdmin

    // =========================================================================
    // NOTE WIDGET
    // =========================================================================
    .directive('note', function(){
        'use strict';
        return {
            restrict: 'EA',
            link: function(scope, element, attrs, ctrl) {
                console.log(attrs.content);
                scope.$parent.actrl.widgets.push(scope);
            },
            templateUrl: "views/notes/create.html",
            transclude: true,
            scope: {

            },
            controller: function($rootScope, $scope, $element) {
                $scope.Delete = function(e) {
                  //remove element and also destroy the scope that element
                  $element.remove();
                  $scope.$destroy();
                },
                $scope.Save = function(e) {
                    let note = {
                        type: "note",
                        content: $scope.note
                    };
                    $scope.$parent.actrl.article.widgets.push(note);
                    
                };
           }
        };      
    })

    .directive('triButton', function(){
        return {
            restrict: 'E',
            replace: true,
            require: "ngModel",
            templateUrl: "views/notes/triTemplate.html",
            link: function(scope, element, attrs, ctrl){
                var setSelected = function(value){
                    var buttons = element.find("button");
                    buttons.removeClass("btn-primary");
                    for( var i = 0; i < buttons.length; i++ ){
                        if(buttons.eq(i).text() == value){
                            buttons.eq(i).addClass("btn-primary");
                        }
                    }
                };
                console.log(scope);
                setSelected(scope.dataValue);
            }
        };
    })
    .directive('inputn1', function(){
        return {
            template: "<div class='panel-body'>Imię: <input  ng-model=actrl.article.note.name /></div>",
        };
    })   

    .directive('exampleBindLeet', function () {
        var leet = {
            a: '4', b: '8', e: '3',
            g: '6', i: '!', l: '1',
            o: '0', s: '5', t: '7',
            z: '2'
        };

        return {
            link: link,
            scope: {
                exampleBindLeet: '='
            }
        };

        function link($scope, $elem, attrs) {
            function convertText() {
                var leetText = $scope.exampleBindLeet.replace(/[abegilostz]/gmi, function (letter) {
                    return leet[letter.toLowerCase()];
                });
         
                $elem.text(leetText);  
            }

            $scope.$watch('exampleBindLeet', convertText);
        }

        
    })