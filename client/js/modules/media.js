materialAdmin

// =========================================================================
// MEDIA ELEMENT
// =========================================================================

.directive('mediaElement', function() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.mediaelementplayer();
        }
    }

})


// =========================================================================
// LIGHTBOX
// =========================================================================

.directive('lightbox', function() {
    return {
        restrict: 'C',
        link: function(scope, element) {
            if (scope.$last) {
                // ng-repeat is completed
                element.lightGallery({
                    enableTouch: true
                });
            }
            // element.lightGallery({
            //     enableTouch: true
            // }); 
        }
    }

})

.directive('lightgallery', function() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            if (scope.$last) {
                // ng-repeat is completed
                element.parent().lightGallery();
            }
        }
    };
});