var materialAdmin = angular.module('materialAdmin', [
    'lbServices',
    'ls.LiveSet', 
    'ls.ChangeStream',
    'infinite-scroll',
    'scrollToFixed',
    'angularFileUpload',
    'ngAnimate',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'oc.lazyLoad',
    'nouislider',
    'ngTable',
    'ngCookies'
]);
 