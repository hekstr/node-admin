materialAdmin.directive('noteWidget', function($timeout) {
	'use strict';
    return {
        restrict: 'AEC',
        transclude: true,
        scope: {
            widget: '='
        },
        link: function(scope, elem, attrs) {
        	
        },
        templateUrl: 'template/noteWidget.html',
        controller: function($rootScope, $scope, $element) {
            $scope.Delete = function(widget) {
                $scope.$parent.actrl.removeWidget(widget);
            	$element.remove();
                $scope.$destroy();
            };
        }
    };
});