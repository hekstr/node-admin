materialAdmin

.directive('sliderWidget', function() {
    'use strict';

    function link(scope, element, attrs) {
    	let content = {
    		slides: []
    	}
    	scope.widget.content = scope.widget.content ? scope.widget.content : content;
    	console.log('scope', scope.widget.content.slides);
        // scope.widget.content.slides = [];
        element.on('$destroy', function() {
            console.log('destroyed');
        });
    }

    return {
        restrict: 'AEC',
        transclude: true,
        scope: {
            widget: '=widget'
        },
        link: link,
        templateUrl: 'template/sliderWidget.html',
        controller: function($scope, $element) {
            $scope.Delete = function(widget) {
                $scope.$parent.actrl.removeWidget(widget);
                $element.remove();
                $scope.$destroy();
            };
            $scope.addNewSlide = function() {
                var newItemNo = $scope.widget.content.slides ? $scope.widget.content.slides.length + 1 : 1;
                $scope.widget.content.slides.push({
                    'id': 'slide' + newItemNo
                });
            };

            $scope.removeSlide = function(slide) {
            	$scope.widget.content.slides.splice($scope.widget.content.slides.indexOf(slide), 1);
            };
        }
    };
});