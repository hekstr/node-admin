materialAdmin

.directive('galleryWidget', function() {
    'use strict';

    function link(scope, element, attrs) {
    	let content = {
    		items: []
    	}
    	scope.widget.content = scope.widget.content ? scope.widget.content : content;
    }

    return {
        restrict: 'AEC',
        transclude: true,
        scope: {
            widget: '=widget'
        },
        link: link,
        templateUrl: 'template/galleryWidget.html',
        controller: function($scope, $element) {
            $scope.Delete = function(widget) {
                $scope.$parent.actrl.removeWidget(widget);
                $element.remove();
                $scope.$destroy();
            };
            $scope.addNewItem = function() {
                var newItemNo = $scope.widget.content.items ? $scope.widget.content.items.length + 1 : 1;
                $scope.widget.content.items.push({
                    'id': 'item' + newItemNo
                });
            };

            $scope.removeItem = function(item) {
            	$scope.widget.content.items.splice($scope.widget.content.items.indexOf(slide), 1);
            };
        }
    };
});