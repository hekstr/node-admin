module.exports = function(grunt) {
    var generatedDocsPath = 'generated_docs';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    paths: ["css"]
                },
                files: {
                    "css/app.css": "less/app.less",
                },
                cleancss: true
            }
        },
        csssplit: {
            your_target: {
                src: ['css/app.css'],
                dest: 'css/app.min.css',
                options: {
                    maxSelectors: 4095,
                    suffix: '.'
                }
            },
        },
        ngtemplates: {
          materialAdmin: {
            src: ['template/**.html', 'template/**/**.html'],
            dest: 'js/templates.js',
            options: {
              htmlmin: {
                    collapseWhitespace: true,
                    collapseBooleanAttributes: true
              }
            }
          }
        },
        watch: {
            a: {
                files: ['less/**/*.less'], // which files to watch
                tasks: ['less', 'csssplit'],
                options: {
                    nospawn: true,
                    livereload: true
                }
            },
            b: {
                files: ['template/**/*.html', '*.html'], // which files to watch
                tasks: ['ngtemplates'],
                options: {
                    nospawn: true,
                    livereload: true
                }
            },
            c: {
                files: ['views/**/*.html'], // which files to watch
                options: {
                    nospawn: true,
                    livereload: true
                }
            },
            d: {
                files: ['js/**/*.js'], // which files to watch
                options: {
                    livereload: true
                }
            }
        },
        loopback_sdk_angular: {
            services: {
              options: {
                input: '../server/server.js',
                output: 'js/services/lb-services.js'
              }
            }
        },
        docular: {
            docular_webapp_target : generatedDocsPath,
            groups: [
              {
                groupTitle: 'LoopBack',
                groupId: 'loopback',
                sections: [
                  {
                    id: 'lbServices',
                    title: 'LoopBack Services',
                    scripts: [ 'js/services/lb-services.js' ]
                  }
                ]
              }
            ],
            showDocularDocs: false,
            showAngularDocs: false
        },
        docularserver: {
            targetDir: generatedDocsPath
        }
    });

    // Load the plugin that provides the "less" task.
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-csssplit');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-serve');

    grunt.loadNpmTasks('grunt-loopback-sdk-angular');
    grunt.loadNpmTasks('grunt-docular');

    // Default task(s).
    grunt.registerTask('default', ['less']);
    grunt.registerTask('develop', [
        'serve',
        'watch'        
    ]);
    grunt.registerTask('document', [
        'loopback_sdk_angular', 'docular'       
    ]);
    grunt.registerTask('service', [
        'loopback_sdk_angular'       
    ]);
    
};
