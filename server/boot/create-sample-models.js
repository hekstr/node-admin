var async = require('async');

module.exports = function(app) {
  // data sources
  var mongoDs = app.dataSources.mongoDs;

  //create all models
  async.parallel({
    users: async.apply(createUsers)
  }, function(err, results) {
    if (err) throw err;
  });

  function createUsers(cb) {
    mongoDs.automigrate('user', function(err) {
      if (err) return cb(err);
      app.models.user.create([
        {email: 'rano@lptg.pl', password: 'qwe123', username:"Rafał Nowak"}
      ], cb);
    });
  }

};