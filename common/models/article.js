var loopbackslug=require("loopback-slug");

module.exports = function(Article) {
	Article.observe('before save', function (ctx, next) {  //!!important
	    loopbackslug.middleware(Article, ctx, {
	      fields: ['title'],
	      slug: "slug"
	    }, function (err) {
	      if (err) return next(err);
	      else next(null);
	    });
	});
};
